#include <stdio.h>

int main(int argc, char **argv)
{
    FILE *fp;
    extern void process(FILE *fp);
    int i;

    if(argc == 1) {
        process(stdin);
    } else {
        for (i = 1; i < argc; i++) {
		    if ((fp = fopen(argv[i], "r")) == NULL) {
			    perror(argv[i]);
                return 1;
            } else {
                process(fp);
                fclose(fp);
            }
        }
    }

    return 0;
}

void process(FILE *fp)
{
    int c;
    int copy = 1;
    while ((c = getc(fp)) != EOF) {
        if (copy) {
            if (c == '<') {
                copy = 0;
            } else {
                putchar(c);
            }
        } else {
            if (c == '>') copy = 1;
        }
    }
}
