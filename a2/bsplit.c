#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

int main(int argc, char **argv)
{
    FILE *fp;
    extern void bsplit(FILE *fp, int n);
    int b = 10;

    int c, status = 0;

    while ((c = getopt(argc, argv, "b:")) != EOF) {
        switch (c) {
        case 'b':
            b = atoi(optarg);
            break;
        default:
            status = 1;
            break;
        }
    }

    if (status || ((argc - optind) > 1) || (b == 0)) {
        fprintf(stderr, "usage: %s [-b size] [file]\n", argv[0]);
        return(status);
    }


    if(optind == argc) {
        bsplit(stdin,b);
    } else {
        if ((fp = fopen(argv[optind], "rb")) == NULL) {
            perror(argv[optind]);
            return 1;
        } else {
            bsplit(fp, b);
            fclose(fp);
        }
    }

    return 0;
}

void bsplit(FILE *fp, int n)
{
    int c;
    int i = 0;
    int file_count = 0;
    FILE *new_file = NULL;
    char filename[4];\

    strcpy(filename, "xaa");
    new_file = fopen(filename, "wb");
    // Loop n times until get EOF
    while ((c = getc(fp)) != EOF) {
        i++;
        putc(c, new_file);
        // If number of bytes read is multiple of n, close file and start a new one     
        if((i % n) == 0) {
            // Close current file
            fclose(new_file);
            // Create file
            file_count++;

            if (filename[1] == 'z' && filename[2] == 'z') {
                fprintf(stderr, "file too large for bsplit algorithm\n");
                exit(1);
            }

            filename[1] = 'a' + file_count/26;
            filename[2] = 'a' + file_count%26;

            new_file = fopen(filename, "wb");
        }
    }
}

