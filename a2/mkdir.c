#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>

int main(int argc, char **argv)
{
	extern void makedir(char *path, int p);
	int pflag = 0;
	int c, status = 0;
	int i = 0;

	while ((c = getopt(argc, argv, "p")) != EOF) {
		switch (c) {
		case 'p':
		    pflag = 1;
		    break;
		default:
		    status = 1;
		    break;
		}
	}
	if (status || (optind == argc)) {
		fprintf(stderr, "usage: %s [-b] path\n", argv[0]);
		return(status);
	}
	for (i = optind; i < argc; i++) {
		makedir(argv[i], pflag);
	}
	return 0;
}

void makedir(char *path, int p)
{
	char *temp = malloc(strlen(path));
	int c, i;
	if (p) {
		for (i = 0; (c = path[i]) != '\0'; i++) {
			if (c == '/') {
				strncpy(temp, path, i);
				mkdir(temp, 0777);
			}
		}
	}
	free(temp);
	if (mkdir(path, 0777)) {
		perror(path);
                exit(1);
	}

}
