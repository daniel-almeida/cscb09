#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include "chatsvr.h"

extern char *memnewline(char *p, int size);  /* finds \r _or_ \n */
extern int isalldigits(char *s);

struct client {
    char handle[80 + 1];
    struct client *next;
};

struct server {
    int serverfd;
    int lines_pending;
    char buf[303];
    char *nextbuf;
    int bytes_in_buf;
    struct client *client_list;
} *serverlist = NULL;

int nservers;

void addclient(int serverindex, char *handle);

void freeMemory() {
	struct client *c;
	struct client *ctemp;
	int i;
	
	for (i = 0; i < nservers; i++) {
		for(c = serverlist[i].client_list; c; c = ctemp) {
			ctemp = c->next;
			free(c);
		}
	}
	free(serverlist);
}

int main(int argc, char **argv)
{
    int i;
    int port;
    int len;
    char *host;
    char *p;
    fd_set fdlist;

    extern void connect_to_server(char *host, int port, int serverindex);
    extern char *read_line_from_server(int serverindex);
    extern void processmessage(int serverindex, char *msg);

    if (argc < 2) {
	   fprintf(stderr, "usage: %s {host port ...} ...\n", argv[0]);
	   return(1);
    }
    /* There can't be more than argc servers, so this is enough space: */
    if ((serverlist = malloc(argc * sizeof(struct server))) == NULL) {
    	fprintf(stderr, "out of memory!\n");
    	freeMemory();
    	exit(1);
    }


    //printf("Initialized servers struct\n");

    // Connect to n servers
    char string[] = "bridge\r\n";
    len = sizeof string - 1;
    nservers = 0;
    for (i = 1; i < argc; i++) {
    	if (isalldigits(argv[i])) {
                if ((port = atoi(argv[i])) <= 0) {
                    fprintf(stderr, "%s: port argument must be a positive integer\n", argv[0]);
                    return(1);
                }
                connect_to_server(host, port, nservers++); /* doesn't return if error */
                // Send handle to servers
                if (write(serverlist[nservers-1].serverfd, string, len) != len) {
				    perror("write");
				    freeMemory();
				    exit(1);
				}
    	} else {
    	    host = argv[i];
    	}
    }
	//printf("Connected to servers\n");
  
	//printf("Entering for to write to all servers\n");
	int maxfd = 0;
    for (;;) {
    	
        //printf("Beginning of while(1)\n");
        FD_ZERO(&fdlist);
        for (i = 0; i < nservers; i++) {
            while (serverlist[i].lines_pending)
            if ((p = read_line_from_server(serverlist[i].serverfd)))
                printf("print inside while: %s\n", p);
            FD_SET(serverlist[i].serverfd, &fdlist);
            if (serverlist[i].serverfd > maxfd) {
            	maxfd = serverlist[i].serverfd;
            }
        }           
        
        FD_SET(0, &fdlist);
        //printf("Already set FD_SET for all servers\n");
        if (select(maxfd + 1, &fdlist, NULL, NULL, NULL) < 0) {
            printf("Error at select\n");
            perror("select");
        } else {
            for (i = 0; i < nservers; i++) {
                //printf("Entering for to check ISSET\n");
                if (FD_ISSET(serverlist[i].serverfd, &fdlist)) {
                    // there is a message from that chatserver           
                    if ((p = read_line_from_server(i))) {
                        // complete msg was received
                        //printf("%s\n", p);
                        //deal with message
                        processmessage(i, p);
                    }
                }      
            }        
        }
    }  
}

void processmessage(int serverindex, char *msg)
{
    
    char sender[81];
    char dest[81];
    int found = 0;
    int i;
    int temp;
    struct client *c = NULL;

    // Get sender's handle
    for (i = 0; msg[i] != '\0'; i++) {
        if (msg[i] == ':') {
            strncpy(sender, msg, i);
            sender[i] = '\0';
            //printf("Sender: %s\n", sender);
            temp = i+2;
            break;
        }
    }

    // Get dest's handle
    for (i = temp; msg[i] != '\0'; i++) {
        if (msg[i] == ',') {
            strncpy(dest, msg+temp, i);
            dest[i-temp] = '\0';
            //printf("Dest: %s\n", dest);
            break;
        }
    }  

    // (do not relay messages from bridge
    if (strcmp(sender, "bridge") && strcmp(sender, "chatsvr")) {
    	//printf("Sender is not bridge\n");
        // loop through all servers looking for that user
        // send the message to all the servers where we found the user
        for (i = 0; i < nservers; i++) {
            for(c = serverlist[i].client_list; c; c = c->next) {
                if (!(strcmp(serverlist[i].client_list->handle, dest))) {
                    // (do not relay messages to the same server from where the message came from)
                    if (i != serverindex){
                        // send message to server i
                        char newmsg[MAXTRANSMISSION];
                        strcpy(newmsg, "");
                        strcat(newmsg, sender);
                        strcat(newmsg, " says: ");
                        strcat(newmsg, msg+temp);
                        //printf("New msg to be sent is %s\n", newmsg);
                        //if(write(serverlist[i].serverfd, newmsg, sizeof newmsg)) {
                        if(write(serverlist[i].serverfd, newmsg, sizeof newmsg) != sizeof newmsg) {
                        	perror("write");
                        }
                    }                 
                }
            }
        }

        // Look at the sender.. do we already have him at the server[i]->usersList ? If not, add
        for(c = serverlist[serverindex].client_list; c; c = c->next) {
            if (!(strcmp(serverlist[serverindex].client_list->handle, sender))) {
            	//printf("Found %s in server %d\n", sender, serverindex);
                found = 1;
            }
        }
        if ((found == 0)) {
        	//printf("Adding %s to server %d\n", sender, serverindex);
            addclient(serverindex, sender);
        }
    }
    
    //printf("%s\n", msg);
}

void addclient(int serverindex, char *handle)
{
    int len;
    struct client *c = malloc(sizeof(struct client));
    if (!c) {
        fprintf(stderr, "out of memory!\n");  /* highly unlikely to happen */
        freeMemory();
        exit(1);
    }
    strcpy(c->handle, handle);
    len = strlen(handle);
    c->handle[len] = '\0';
    c->next = serverlist[serverindex].client_list;
    serverlist[serverindex].client_list = c;
}

char *memnewline(char *p, int size)  /* finds \r _or_ \n */
	/* This is like min(memchr(p, '\r'), memchr(p, '\n')) */
	/* It is named after memchr().  There's no memcspn(). */
{
    for (; size > 0; p++, size--)
	if (*p == '\r' || *p == '\n')
	    return(p);
    return(NULL);
}


int isalldigits(char *s)
{
    for (; *s; s++)
	if (!isdigit(*s))
	    return(0);
    return(1);
}

void connect_to_server(char *host, int port, int serveridx)
{
    struct hostent *hp;
    struct sockaddr_in r;
    char *p;
    extern char *read_line_from_server(int serverindex);
    
    serverlist[serveridx].lines_pending = 0;
    serverlist[serveridx].nextbuf = NULL;
    serverlist[serveridx].client_list = NULL;
    serverlist[serveridx].bytes_in_buf = 0;

    if ((hp = gethostbyname(host)) == NULL) {
        fprintf(stderr, "%s: no such host\n", host);
        freeMemory();
        exit(1);
    }
    if (hp->h_addr_list[0] == NULL || hp->h_addrtype != AF_INET) {
        fprintf(stderr, "%s: not an internet protocol host name\n", host);
        freeMemory();
        exit(1);
    }

    if ((serverlist[serveridx].serverfd = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
        perror("socket");
        freeMemory();
        exit(1);
    }

    r.sin_family = AF_INET;
    memcpy(&r.sin_addr, hp->h_addr_list[0], hp->h_length);
    r.sin_port = htons(port);

    if (connect(serverlist[serveridx].serverfd, (struct sockaddr *)&r, sizeof r) < 0) {
        perror("connect");
        freeMemory();
        exit(1);
    }

    while ((p = read_line_from_server(serveridx)) == NULL)
    ; /* i.e. loop until we get an entire line */
    if (strcmp(p, CHATSVR_ID_STRING)) {
        fprintf(stderr, "That is not a 'chatsvr'\n");
        freeMemory();
        exit(1);
    }
}

/* This function is guaranteed to do at most one read(). */
char *read_line_from_server(int serverindex)
{
    //static char buf[MAXTRANSMISSION + 3];
    //static char *nextbuf = NULL;
    //static int bytes_in_buf = 0;
    int len;
    char *p;
    
    /*
     * If we returned a line last time, that's at the beginning of buf --
     * move the rest of the string over it.  The bytes_in_buf value has
     * already been adjusted.
     */
    if (serverlist[serverindex].nextbuf) {
        memmove(serverlist[serverindex].buf, serverlist[serverindex].nextbuf, serverlist[serverindex].bytes_in_buf);
        serverlist[serverindex].nextbuf = NULL;
    }

    /* Do a read(), unless we already have a whole line. */
    if (!memnewline(serverlist[serverindex].buf, serverlist[serverindex].bytes_in_buf)) {
    if ((len = read(serverlist[serverindex].serverfd, serverlist[serverindex].buf + serverlist[serverindex].bytes_in_buf,
            sizeof serverlist[serverindex].buf - serverlist[serverindex].bytes_in_buf - 1)) < 0) {
        perror("read");
        freeMemory();
        exit(1);
    }
    if (len == 0) {
        printf("Server shut down.\n");
        freeMemory();
        exit(0);
    }
    serverlist[serverindex].bytes_in_buf += len;
    }

    /* Now do we have a whole line? */
    if ((p = memnewline(serverlist[serverindex].buf, serverlist[serverindex].bytes_in_buf))) {
        serverlist[serverindex].nextbuf = p + 1;  /* the next line if the newline is one byte */
        /* but if the newline is \r\n... */
        if (serverlist[serverindex].nextbuf < serverlist[serverindex].buf + serverlist[serverindex].bytes_in_buf && *p == '\r' && *(p+1) == '\n')
            serverlist[serverindex].nextbuf++;  /* then skip the \n too */
        /*
         * adjust bytes_in_buf for next time.  Data moved down at the
         * beginning of the next read_line_from_server() call.
         */
        serverlist[serverindex].bytes_in_buf -= serverlist[serverindex].nextbuf - serverlist[serverindex].buf;
        *p = '\0';  /* we return a nice string */

        /* Is there a subsequent line already waiting? */
        serverlist[serverindex].lines_pending = !!memnewline(serverlist[serverindex].nextbuf, serverlist[serverindex].bytes_in_buf);

        return(serverlist[serverindex].buf);
    }

    /*
     * Is the buffer full even though we don't yet have a whole line?
     * This shouldn't happen if the server is following the protocol, but
     * still we don't want to infinite-loop over this.
     */
    if (serverlist[serverindex].bytes_in_buf == sizeof serverlist[serverindex].buf - 1) {
        serverlist[serverindex].buf[sizeof serverlist[serverindex].buf - 1] = '\0';
        serverlist[serverindex].bytes_in_buf = 0;
        serverlist[serverindex].lines_pending = 0;
        return(serverlist[serverindex].buf);  /* needn't set nextbuf because there's nothing to move */
    }

    /* No line yet.  Please try again later. */
    return(NULL);
}
