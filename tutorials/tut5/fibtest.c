#include <stdio.h>
#include <stdlib.h>

int main(int argc, char **argv)
{
	int fib[100];
	int n, i, ret;

	if (argc != 2) {
		printf("usage: fib n");
		return(1);
	}
	n = atoi(argv[1]);
	if (n >= 100) {
		return(1);// Error
	}
	fib[0] = 0;
	fib[1] = 1;
	if (n == 0) {
		ret = fib[0];
	} else { 
		if (n == 1) {
			ret = fib[1];
		} else {
			for (i = 2; i <= n; i++) {
				fib[i] = fib[i-1] + fib[i-2];
			}
			ret = fib[n];
		}
	}
	printf("The fibonacci result is %d\n", ret); 
	for(i=0; i <= n; i++) {
		printf("\nFibonacci[%d]: %d", i, fib[i]);
	}
	return(0);
}
