#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <time.h>
#include <dirent.h>
#include <unistd.h>

int main()
{
	int inode;
	struct stat statbuf;
	DIR *dp;
    struct dirent *p;

	if (lstat(".", &statbuf)) {
	    perror(".");
	    return(1);
	}

	while ((inode = statbuf.st_ino) != 2) {
		chdir("..");
		if ((dp = opendir(".")) == NULL) {
			perror(".");
			return(1);
		}
		p = readdir(dp);
		while (p->d_ino != inode) {
			p = readdir(dp);
		}
		printf("%s\n", p->d_name);
		closedir(dp);
		if (lstat(".", &statbuf)) {
			perror(".");
			return(1);
		}
	}

	printf("Finally... this is the root directory\n");
	return 0;
}
