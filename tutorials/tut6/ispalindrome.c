#include <stdio.h>
#include <string.h>
#include <ctype.h>

int main(int argc, char **argv)
{
	char *i;
	char *j;

	if (argc != 2) {
		fprintf(stderr, "usage: %s string\n", argv[0]);
		return(1); 	
	}

	i = argv[1];
	j = argv[1] + strlen(argv[1]) - 1;

	while (i < j) {
		while (!isalnum(*i)) {
			i++;
		}
		while (!isalnum(*j)) {
			j--;
		}
		printf("i is: %c\n", *i);
		printf("j is: %c\n", *j);
		if (tolower(*i) != tolower(*j)) {
			return(1);
		} 
		i++;
		j--;
	}
	return(0);
}
