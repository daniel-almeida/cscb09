#include <stdio.h>
#include <string.h>
#include <ctype.h>

int main(int argc, char **argv)
{
	char *i;
	char *j;
	extern int get_length(char *str);

	if (argc != 2) {
		fprintf(stderr, "usage: %s string\n", argv[0]);
		return(1); 	
	}

	i = argv[1];
	j = argv[1] + strlen(argv[1]) - 1;
	//printf("length is %d\n", strlen(argv[1])); // REMOVE
	printf("i starts as %c\n", *i); // REMOVE
	printf("j starts as %c\n", *j); // REMOVE
	while (i < j) {
		while (!isalnum(*i)) {
			i++;
		}
		while (!isalnum(*j)) {
			j--;
		}
		printf("i is: %c\n", *i);
		printf("j is: %c\n", *j);
		if (tolower(*i) != tolower(*j)) {
			printf("Is not palindrome.\n"); // Remove before submitting
			return(1);
		} 
		i++;
		j--;
	}
	printf("Is palindrome.\n"); // Remove before submitting
	return(0);
}

int get_length(char *str)
{
	int str_length = 0;
	while (*str != '\0') {
		str_length++;
		str++;
	}
	return str_length;
}
