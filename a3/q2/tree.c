#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>

#define STRINGSIZE 10

int verbose = 0;

extern int benode(int fromparent, int toparent, int key, char *value);
extern int parse_in_place(char *s);
extern void formatbuf(char *buf, int key, char *value);
extern void mystrncpy(char *s, char *t);
extern char* getvalue(char* buf);
extern int getkey(char* buf);

int main(int argc, char **argv)
{
    int c, status = 0;

    while ((c = getopt(argc, argv, "v")) != EOF) {
        switch (c) {
        case 'v':
            verbose = 1;
            break;
        default:
            status = 1;
        }
    }
    if (status || optind + 2 != argc) {
        fprintf(stderr, "usage: %s [-v] key value\n", argv[0]);
        return(1);
    }
    benode(-1, -1, atoi(argv[optind]), argv[optind+1]);
    return(0);
}


int benode(int fromparent, int toparent, int key, char *valuearg)
{
	int leftpid = -1;
	int rightpid = -1;
	char buf[sizeof(int)+STRINGSIZE];
	char* requestvalue = valuearg;
	int requestkey;
	int pipe_left_to_parent[2];
	int pipe_left_from_parent[2];
	int pipe_right_to_parent[2];
	int pipe_right_from_parent[2];

    /* get a pipe (buffer and fd pair) from the OS */

    if (pipe(pipe_left_to_parent)) {
		perror("pipe left to parent");
		exit(127);
    }

	if (pipe(pipe_right_to_parent)) {
		perror("pipe right to parent");
		exit(127);
    }

	if (pipe(pipe_left_from_parent)) {
		perror("pipe left from parent");
		exit(127);
    }

	if (pipe(pipe_right_from_parent)) {
		perror("pipe right from parent");
		exit(127);
    }
    
    if (fromparent != -1) {
    	write(toparent, buf, sizeof(int)+STRINGSIZE);
    }

    while (1) {
        if (fromparent < 0) { // IS ROOT !
            if (fgets(buf, sizeof buf, stdin) == NULL || parse_in_place(buf)) {
				// shut everything down and exit ...
				if (leftpid == -1) {
					close(pipe_left_to_parent[0]);
					close(pipe_left_from_parent[1]);
				}
				close(pipe_left_to_parent[1]);
				close(pipe_left_from_parent[0]);
				
				if (rightpid > 0) {
					close(pipe_right_to_parent[0]);	
					close(pipe_right_from_parent[1]);
				}
				close(pipe_right_to_parent[1]);
				close(pipe_right_from_parent[0]);
				
				while(wait(NULL)>0) 
				break;
			}
			
			requestkey = getkey(buf);

			if ((buf[sizeof(int)]) == '\0') { // SEARCH		
				if (key == requestkey){
					printf("%d %s\n", requestkey, requestvalue);
					fflush(stdout);
				} else {
					if (requestkey < key) {
						if (leftpid > 0) {
							// Send key through pipe to left node
							write(pipe_left_from_parent[1], buf, sizeof(int)+STRINGSIZE);
							read(pipe_left_to_parent[0], buf, sizeof(int)+STRINGSIZE);
						}
					} else {
						if (rightpid > 0) {
							// Send key through pipe to right node
							write(pipe_right_from_parent[1], buf, sizeof(int)+STRINGSIZE);
							read(pipe_right_to_parent[0], buf, sizeof(int)+STRINGSIZE);
							
						}
					}
					printf("%d %s\n", getkey(buf), getvalue(buf));
					fflush(stdout);
				}
			} else { // NEW NODE
				requestkey = getkey(buf);
				if(requestkey == key) {
					strcpy(requestvalue, buf + sizeof(int));
					printf("%d %s\n", requestkey, requestvalue);
					fflush(stdout);
				}
				// Create child				
				if (requestkey < key) { // CHILD LEFT
					if (leftpid > 0) {
						// Send key through pipe to left node
						write(pipe_left_from_parent[1], buf, sizeof(int)+STRINGSIZE);
						read(pipe_left_to_parent[0], buf, sizeof(int)+STRINGSIZE);
						
					} else {
						switch (leftpid = fork()) {
						case -1:
							perror("fork");
							exit(127);
						case 0:
							/* child process */
							close(pipe_left_to_parent[0]);
							close(pipe_left_from_parent[1]);
							benode(pipe_left_from_parent[0], pipe_left_to_parent[1], requestkey, requestvalue);
						default:
							/* parent process */
							close(pipe_left_to_parent[1]);
							close(pipe_left_from_parent[0]);

							/* Read */
							read(pipe_left_to_parent[0], buf, sizeof(int)+STRINGSIZE);
						}
					}
					printf("%d %s\n", getkey(buf), getvalue(buf));
					fflush(stdout);	
				} else {
					if (requestkey > key) { // CHILD RIGHT
						if (rightpid > 0) {
							// Send key through pipe to left node
							write(pipe_right_from_parent[1], buf, sizeof(int)+STRINGSIZE);
							read(pipe_right_to_parent[0], buf, sizeof(int)+STRINGSIZE);
						} else {
							switch (rightpid = fork()) {
							case -1:
								perror("fork");
								exit(127);
							case 0:
								/* child process */
								close(pipe_right_to_parent[0]);
								close(pipe_right_from_parent[1]);
								benode(pipe_right_from_parent[0], pipe_right_to_parent[1], requestkey, requestvalue);
							default:
								/* parent process */
								close(pipe_right_to_parent[1]);
								close(pipe_right_from_parent[0]);
								/* Read */
								read(pipe_right_to_parent[0], buf, sizeof(int)+STRINGSIZE);
							}							
						}
					}
					printf("%d %s\n", getkey(buf), getvalue(buf));
					fflush(stdout);
				}
			}
        } else { // NOT ROOT!
			// read from "fromparent"
			if (read(fromparent, buf, sizeof(int)+STRINGSIZE) < 0) { // EOF
				if (leftpid == -1) {
					close(pipe_left_to_parent[0]);
					close(pipe_left_from_parent[1]);
				}
				close(pipe_left_to_parent[1]);
				close(pipe_left_from_parent[0]);
				
				if (rightpid > 0) {
					close(pipe_right_to_parent[0]);
					close(pipe_right_from_parent[1]);
				}
				close(pipe_right_to_parent[1]);
				close(pipe_right_from_parent[0]);
				while(wait(NULL)>0) 
				break;
			}

			requestkey = getkey(buf);
	
            if ((buf[sizeof(int)]) == '\0') {	// SEARCH	
				if (key == requestkey) { // FOUND KEY
					strcpy(buf + sizeof(int), requestvalue);	
				} else {
					if (requestkey < key) {
						if (leftpid > 0) {
							// Send key through pipe to left node
							write(pipe_left_from_parent[1], buf, sizeof(int)+STRINGSIZE);
							read(pipe_left_to_parent[0], buf, sizeof(int)+STRINGSIZE);
						}
					} else {
						if (rightpid > 0) {
							// Send key through pipe to right node
							write(pipe_right_from_parent[1], buf, sizeof(int)+STRINGSIZE);
							read(pipe_right_to_parent[0], buf, sizeof(int)+STRINGSIZE);
						}
					}
				}
				write(toparent, buf, sizeof(int)+STRINGSIZE);
			} else {
				requestkey = getkey(buf);
				mystrncpy(requestvalue, getvalue(buf));
				if(requestkey == key) {
					mystrncpy(requestvalue, getvalue(buf));
					printf("%d %s\n", getkey(buf), getvalue(buf));
					fflush(stdout);
				} else {
					// Create child				
					if (requestkey < key) { // CHILD LEFT
						if (leftpid > 0) {
							// Send key through pipe to left node
							write(pipe_left_from_parent[1], buf, sizeof(int)+STRINGSIZE);
							read(pipe_left_to_parent[0], buf, sizeof(int)+STRINGSIZE);
						} else {
							switch (leftpid = fork()) {
							case -1:
								perror("fork");
								exit(127);
							case 0:
								/* child process */
								close(pipe_left_to_parent[0]);
								close(pipe_left_from_parent[1]);
								benode(pipe_left_from_parent[0], pipe_left_to_parent[1], requestkey, requestvalue);
							default:
								/* parent process */
								close(pipe_left_to_parent[1]);
								close(pipe_left_from_parent[0]);

								/* Read */
								read(pipe_left_to_parent[0], buf, sizeof(int)+STRINGSIZE);
							}
						}
					
					} else {
						if (requestkey > key) { // CHILD RIGHT
							if (rightpid > 0) {
								// Send key through pipe to right node
								write(pipe_right_from_parent[1], buf, sizeof(int)+STRINGSIZE);
								read(pipe_right_to_parent[0], buf, sizeof(int)+STRINGSIZE);
							} else {
								switch (rightpid = fork()) {
								case -1:
									perror("fork");
									exit(127);
								case 0:
									/* child process */
									close(pipe_right_to_parent[0]);
									close(pipe_right_from_parent[1]);
									benode(pipe_right_from_parent[0], pipe_right_to_parent[1], requestkey, requestvalue);
								default:
									/* parent process */
									close(pipe_right_to_parent[1]);
									close(pipe_right_from_parent[0]);
									/* Read */
									read(pipe_right_to_parent[0], buf, sizeof(int)+STRINGSIZE);
								}
							}
						}
					}
				}
				write(toparent, buf, sizeof(int)+STRINGSIZE); // SEND buf to parent
			}
        }
    }
    return 0;
}


/*
 * Turn interactive input from the user into our standard char array of
 *     length sizeof(int)+STRINGSIZE, modifying the char array in place.
 * Argument pointed to must be big enough.
 */
int parse_in_place(char *s)
{
    int key;
    char value[STRINGSIZE], *p;

    key = atoi(s);

    if ((p = strchr(s, ' ')) == NULL) {
        value[0] = '\0';
    } else {
        while (*p && *p == ' ')
            p++;
        mystrncpy(value, p);
        if ((p = strchr(value, '\n')))
            *p = '\0';
    }

    formatbuf(s, key, value);
    return(0);
}


void formatbuf(char *buf, int key, char *value)
{
    memcpy(buf, &key, sizeof(int));
    strcpy(buf + sizeof(int), value);
}


void mystrncpy(char *s, char *t)
{
    strncpy(s, t, STRINGSIZE);
    s[STRINGSIZE - 1] = '\0';
}

int getkey(char* buf)
{
	return (*(int *)buf);
}

char* getvalue(char* buf)
{
	return buf + sizeof(int);
}
