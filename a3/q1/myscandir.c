#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <time.h>
#include <dirent.h>
#include <unistd.h>
#include "myscandir.h"

int myscandir(char *dir, struct dirent ***namelist)
{
	int dirent_count = 0;
	int namelist_space = 3;
	int namelist_capacity = 3;
	DIR *dp;
    struct dirent *p;


	printf("Dir is %s\n", dir);
	if ((dp = opendir(dir)) == NULL) {
		return (-1);
	}
	if ((*namelist = malloc(3 * sizeof(struct dirent))) == NULL) {
		return (-1);
	}
	p = readdir(dp);
	while (p != NULL) {
		if (!namelist_space) {
            namelist_capacity += 3;
            namelist_space = 3;
            if ((realloc(*namelist, namelist_capacity*sizeof(struct dirent)) == NULL)) {
                return (-1);
            }
        }

		// Deal with each dir (p is the dirent)

		(*namelist)[dirent_count] = p;
		dirent_count++; // Increment count of entries
		namelist_space--; // Decrement space available for new entries
		p = readdir(dp);
	}
    closedir(dp);
	myfreescandir(*namelist, dirent_count);
	return dirent_count; // Return number of entries found
}

void myfreescandir(struct dirent **namelist, int n) 
{
	free(namelist);
}
