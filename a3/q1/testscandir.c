#include <stdio.h>
#include <dirent.h>
#include "myscandir.h"

int main(int argc, char **argv)
{
    char *dir = (argc == 2) ? argv[1] : ".";
    int n, i;
    struct dirent **namelist;

    n = myscandir(dir, &namelist);
    if (n < 0) {
		perror(dir);
		return(1);
    }
    for (i = 0; i < n; i++)
		printf("%s\n", namelist[i]->d_name);

    return(0);
}
